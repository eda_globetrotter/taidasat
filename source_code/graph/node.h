/**
 * taidasat: An AIG-based SAT solver.
 *
 * Graph package:
 * Module to implement all graph data structures in our AIG-based
 * circuit SAT solver.
 *
 * node class:
 * The Node class of the Graph module for the AIG-based
 * SAT solver defines the generic Node type for the graph class
 * template.
 *
 * It includes the common properties of nodes that belong
 * to directed graphs.
 * #	Name of the node.
 * #	list of source nodes: incoming edges.
 * #	list of destination nodes: outgoing edges.
 *
 * It includes the common functionality of nodes.
 * #	get_node_id(): Get the ID of the node.
 * #	set_node_id(): Set the ID of the node.
 * #	get_outgoing_destn_nodes(): Get the list of
 *		destination nodes.
 * #	add_outgoing_destn_node(): Add a destination node.
 * #	get_incoming_src_nodes(): Get the list of
 *		source nodes.
 * #	add_incoming_src_node(): Add a source node.
 *
 *
 *
 * Notes:
 * #	Node (nodes) and vertex (plural, vertices) shall
 *		be used interchangeably.
 * #	Edge (plural, edges) and arc (plural, arcs) shall
 *		be used interchangeably.
 * #	Graphs shall be represented with adjacency lists
 *		by default. They can be extended and represented
 *		in other ways.
 *
 *
 *
 * References:
 * [Insert some references for graph data structures, and
 *	graph representations in software.]
 *
 *
 * The MIT License (MIT).
 * Copyright	(C)	<2013>	<Chiao Hsieh and Zhiyang Ong>
 * @author Zhiyang Ong
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */


// Import Header files from the C++ STL
#include <vector>

// Import Header files from the other modules of taidasat.
// Graph module
//#include "node_id.h"

// Use standard namespace.
using namespace std;

// =======================================================================

template <class T>
class node {
	// Properties of the Node class.
	
	// Name of the node, which is used to uniquely identify the node.
	T node_name;
	// List of source nodes, which represent incoming edges.
	vector<T> src_nodes;
	// List of destination nodes, which represent outgoing edges.
	vector<T> destn_nodes;
	
public:
	// -----------------------------------------------------
	
	// Define headers for functions...
	
	// -----------------------------------------------------
	
	// Accessor functions.
	
	// Get the name of the node.
	T get_node_name();
	// Get the list of destination nodes.
	vector<T> get_outgoing_destn_nodes();
	// Get the list of source nodes.
	vector<T> get_incoming_src_nodes();
	
	// -----------------------------------------------------
	
	// Mutator functions.
	// Set the name of the node.
	void set_node_name(T name);
	// Add a destination node.
	void add_outgoing_destn_node(T destn_node);
	// Add a source node.
	void add_incoming_src_node(T src_node);
};















