/**
 * taidasat: An AIG-based SAT solver.
 *
 * Graph/AIG package:
 * Module to implement all graph data structures in our AIG-based
 * circuit SAT solver.
 *
 * aig_node class:
 * The AIG_Node class of the Graph/AIG submodule for the AIG-based
 * SAT solver defines the AIG_Node type. It derives from the
 * generic Node type for the graph class template.
 *
 * It includes the common properties of AIG nodes that
 * belong to directed graphs.
 * #	Name of the node.
 * #	list of source nodes: incoming edges.
 * #	list of destination nodes: outgoing edges.
 *
 * It contains AIG-specific properties:
 * #	ID/index of the AIG node in the adjacency list
 *
 *
 * It includes the common functionality of nodes.
 * #	get_node_id(): Get the ID of the node.
 * #	set_node_id(): Set the ID of the node.
 * #	get_outgoing_destn_nodes(): Get the list of
 *		destination nodes.
 * #	add_outgoing_destn_node(): Add a destination node.
 * #	get_incoming_src_nodes(): Get the list of
 *		source nodes.
 * #	add_incoming_src_node(): Add a source node.
 *
 * It includes AIG-specific functionality:
 * #	set_node_id(): Set the ID of the node.
 * #	get_node_id(): Get the ID of the node.
 *
 * Notes:
 * #	Node (nodes) and vertex (plural, vertices) shall
 *		be used interchangeably.
 * #	Edge (plural, edges) and arc (plural, arcs) shall
 *		be used interchangeably.
 * #	For an AIG, all nodes are AND gates.
 * #	For inverters in AIGs, label the outgoing edges of
 *		AND gates with (bool) inverter = true.
 * #	That is, an inverter is placed either before/after
 *		an AND gate. So, labeling the outgoing edges of
 *		the AND gates would suffice in specifying the
 *		inverters in the AIG.
 *
 *
 *
 *
 *
 *
 *
 * The MIT License (MIT).
 * Copyright	(C)	<2013>	<Chiao Hsieh and Zhiyang Ong>
 * @author Zhiyang Ong
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

// Import Header files from the C++ STL
#include <string>
#include <vector>
//#include <pair>
//#include <utility>      // std::pair

// Import Header files from the other modules of taidasat.
// Graph module
#include "../node.h"
#include "aig_node.h"

// Use standard namespace.
using namespace std;

// =======================================================================

// Defining typedefs.

/**
  * Implement the pair<aig_node, bool> as a class for a better
  * software architectural design.
  */
//typedef pair<aig_node, bool> gate_type;

// =======================================================================

// Initialization of (string) constants.

// =======================================================================

// Define the static variables of the class aig_node.

// ==============================================================

// Default constructor
aig_node::aig_node() {
	// Default AIG node has no name...
	node_name = NULL;
	// Nor node ID... Set it to the maximum value.
	node_id = ULONG_MAX;
	// List of source nodes representing incoming edges.
	src_nodes = nullptr_t;
	// List of destination nodes representing outgoing edges.
	destn_nodes = nullptr_t;
}



// Standard constructor
aig_node(T name, unsigned int nid, vector<T> s_nodes,
	vector<T> t_nodes) {
	
	// Set the identifier for the AIG node.
	node_name = name;
	// Set its node ID.
	node_id = nid;
	// List of source nodes representing incoming edges.
	src_nodes = s_nodes;
	// List of destination nodes representing outgoing edges.
	destn_nodes = t_nodes;
}

// ==============================================================


// Accessor functions.
	
/**
 * Function to get the name of the node.
 * @param - None.
 * @return - The name of the node.
 */
T aig_node::get_node_name() {
	return node_name;
}

/**
 * Function to get the ID of the node.
 * @param - None.
 * @return - The ID of the node.
 */
unsigned int aig_node::get_node_id() {
	return node_id;
}

/**
 * Function to get the list of destination nodes.
 * @param - None.
 * @return - The list of destination nodes.
 */
vector<aig_node> aig_node::get_outgoing_destn_nodes() {
	
}

// Get the list of source nodes.
vector<aig_node> aig_node::get_incoming_src_nodes();

// -----------------------------------------------------

// Mutator functions.

// Set the name of the node.
void aig_node::set_node_name(T name);

// Set the ID of the node.
void aig_node::set_node_id(unsigned int nid);

// Add a destination node.
void aig_node::add_outgoing_destn_node(aig_node destn_node);

// Add a source node.
void aig_node::add_incoming_src_node(aig_node src_node) {
	
}
















