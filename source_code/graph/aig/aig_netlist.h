/**
 * taidasat: An AIG-based SAT solver.
 *
 * Test suite for taidasat.
 *
 * The MIT License (MIT).
 * Copyright	(C)	<2013>	<Chiao Hsieh and Zhiyang Ong>
 * @author Zhiyang Ong
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *
 *
 *
 * The Graph class of the Graph module for the AIG-based
 * SAT solver.
 * It includes the common properties of directed graphs.
 * #	number_of_nodes
 * #	number_of_edges
 * #	container to store the graph: NOT specified, so
 *		that developers can implement the graph using
 *		various graph representations.
 *
 * It includes the common functionality of graphs.
 * #	get_number_nodes(): |V|
 * #	get_number_edges(): |E|
 * #	[common graph traversal algorithms that will be
 *		shared]
 *
 *
 *
 * Notes:
 * #	Node (nodes) and vertex (plural, vertices) shall be used
 *		interchangeably.
 * #	Edge (plural, edges) and arc (plural, arcs) shall be used
 *		interchangeably.
 */


// Import Header files from the other modules of taidasat.
// Graph module
#include "node_id.h"















